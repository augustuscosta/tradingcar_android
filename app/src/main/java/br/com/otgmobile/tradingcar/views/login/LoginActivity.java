package br.com.otgmobile.tradingcar.views.login;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.service.DeviceIdService;
import br.com.otgmobile.tradingcar.service.SessionService;
import br.com.otgmobile.tradingcar.service.UserService;
import br.com.otgmobile.tradingcar.views.home.HomeActivity_;


@EActivity(R.layout.activity_login)
public class  LoginActivity extends AppCompatActivity {

    @ViewById(R.id.container)
    FrameLayout container;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @Bean
    DeviceIdService deviceIdService;

    @Bean
    SessionService sessionService;

    @Bean
    UserService userService;


    private Fragment currentFragment;

    private LoginFragment loginFragment;

    private RecoverPasswordFragment recoverPasswordFragment;

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);

        if (sessionService.getCurrentSession() != null) {
            callHomeActivity();

        } else {
            goToLogin();
        }

    }


    private LoginFragment getLogiFragment(){

        if(null == loginFragment){
            loginFragment = new LoginFragment_();
            loginFragment.setLoginActivity(this);
        }

        return loginFragment;

    }

    private RecoverPasswordFragment getRecoverPasswordFragment(){

        if(null == recoverPasswordFragment){
            recoverPasswordFragment = new RecoverPasswordFragment_();
            recoverPasswordFragment.setLoginActivity(this);
        }

        return recoverPasswordFragment;

    }


    public void goToRecoverPassword() {
        changeFragment(getRecoverPasswordFragment());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void goToLogin() {
        changeFragment(getLogiFragment());
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void changeFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
        currentFragment = fragment;
    }

     public void callHomeActivity() {
        HomeActivity_.intent(this).start();
        finish();
    }

    public void onBackPressed() {
        if(currentFragment == recoverPasswordFragment){
            goToLogin();
        }else{
            finish();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
