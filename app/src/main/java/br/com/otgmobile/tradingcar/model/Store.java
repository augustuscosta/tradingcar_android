package br.com.otgmobile.tradingcar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by augustuscosta on 03/05/17.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Store {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty("contact_name")
    private String contactName;

    @DatabaseField
    @JsonProperty
    private String email;

    @DatabaseField
    @JsonProperty
    private String phone;

    @DatabaseField
    @JsonProperty
    private String cnpj;

    @DatabaseField
    @JsonProperty
    private String address;

    @DatabaseField
    @JsonProperty("pay_day")
    private Integer payDay;

    @DatabaseField
    @JsonProperty
    private Boolean blocked;

    @DatabaseField
    @JsonProperty
    private String message;

    @ForeignCollectionField
    @JsonProperty
    private Collection<Trade> trades;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPayDay() {
        return payDay;
    }

    public void setPayDay(Integer payDay) {
        this.payDay = payDay;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Collection<Trade> getTrades() {
        return trades;
    }

    public void setTrades(Collection<Trade> trades) {
        this.trades = trades;
    }
}
