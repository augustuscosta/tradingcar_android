package br.com.otgmobile.tradingcar.tasks;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import br.com.otgmobile.tradingcar.service.DeviceIdService;

/**
 * Created by augustuscosta on 25/08/16.
 */
@EService
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Bean
    DeviceIdService deviceIdService;

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        deviceIdService.updateDeviceId(refreshedToken);
        deviceIdService.sendDeviceId();
    }

}
