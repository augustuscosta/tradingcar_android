package br.com.otgmobile.tradingcar.views.trade;

/**
 * Created by augustuscosta on 03/05/17.
 */

public interface ImageRecycleViewNotifier {

    void notifyDataChange();
}
