package br.com.otgmobile.tradingcar.util;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Thialyson on 12/08/16.
 */
public class ValidateUtil {

    public static boolean isNotNullEmail(EditText editText, String msgError) {

        if (!isValidEmail(editText.getText())) {
            editText.requestFocus();
            editText.setText("");
            editText.setError(msgError);
            return false;

        } else {
            return true;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {

        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean checkConfirmPassword(EditText editText, EditText editText2, String msgError) {

        if (!editText.getText().toString().equals(editText2.getText().toString())) {
            editText2.requestFocus();
            editText2.setError(msgError);
            return false;
        } else {
            return true;
        }
    }

    public static boolean checkIfAllAreTrue(List<Boolean> list) {

        for (int i = 0; i < list.size(); i++) {
            if (!list.get(i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkInterval(EditText editText, Integer min, Integer max, String msgError) {

        if (editText.length() < min || editText.length() > max) {
            editText.requestFocus();
            editText.setError(msgError);
            return false;

        } else {
            return true;
        }
    }

    public static boolean checkCpfCnpj(EditText editText, Integer min, Integer max, String msgError) {

        if (editText.length() != max && editText.length() != min) {
            editText.requestFocus();
            editText.setError(msgError);
            return false;
        } else {
            return true;
        }
    }

    public static boolean checkImageView(String msgError, boolean bool, Context context) {

        if (!bool) {
            Toast.makeText(context, msgError, Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    public static boolean checkIsEmpty(EditText editText, String msgError) {

        if (editText.getText().length() == 0) {
            editText.requestFocus();
            editText.setText("");
            editText.setError(msgError);
            return false;
        } else {
            return true;
        }
    }
}
