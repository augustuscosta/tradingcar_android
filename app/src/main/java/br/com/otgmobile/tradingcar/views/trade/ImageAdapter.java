package br.com.otgmobile.tradingcar.views.trade;

import android.content.Context;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.tradingcar.R;

/**
 * Created by augustuscosta on 02/05/17.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private List<Uri> images = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private Context context;
    private ImageRecycleViewNotifier notifier;

    public ImageAdapter(Context context, ImageRecycleViewNotifier notifier) {
        if(context != null) {
            this.context = context;
            this.notifier = notifier;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    public void addImage(Uri uri){
        images.add(uri);
    }

    public List<Uri> getImages() {
        return images;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        setImage(holder, position);
    }

    private void setImage(ViewHolder holder, final int position) {
        holder.imageView.setImageURI(images.get(position));
        holder.deletePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                images.remove(position);
                notifyDataSetChanged();
                notifier.notifyDataChange();
            }
        });
    }


    @Override
    public int getItemCount() {
        return images.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        ImageButton deletePhotoButton;


        ViewHolder(final View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            deletePhotoButton = (ImageButton) itemView.findViewById(R.id.deletePhotoButton);
        }
    }
}
