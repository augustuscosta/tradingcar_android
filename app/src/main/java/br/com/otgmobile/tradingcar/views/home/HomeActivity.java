package br.com.otgmobile.tradingcar.views.home;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.User;
import br.com.otgmobile.tradingcar.service.SearchService;
import br.com.otgmobile.tradingcar.service.SessionService;
import br.com.otgmobile.tradingcar.service.UserService;
import br.com.otgmobile.tradingcar.util.DialogUtil;
import br.com.otgmobile.tradingcar.views.login.LoginActivity_;
import br.com.otgmobile.tradingcar.views.trade.TradesFragment_;
import br.com.otgmobile.tradingcar.views.trade.WantBuyTradesFragment_;
import br.com.otgmobile.tradingcar.views.trade.WantSellTradesFragment_;

@EActivity(R.layout.activity_home)
public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @Bean
    SessionService sessionService;

    @Bean
    UserService userService;

    @Bean
    SearchService searchService;

    private Fragment currentFragment;

    private boolean doubleBackToExitPressedOnce = false;

    private TextView nameUserLogged;

    private User userLogged;

    private View header;

    private boolean tradesFragmentOn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchService.storedSearch(query);
            if(!tradesFragmentOn)
                setTradesFragment();
        }
    }

    @AfterViews
    void afterViews() {
        setMenu();
        requestCurrentUser();
    }

    @Background
    void requestCurrentUser() {
        try {
            userService.getCurrentUser();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        getFromDatabase();
    }

    @UiThread
    void getFromDatabase() {
        userLogged = userService.getLocalCurrentUser();
        setNavigationView();
        loadComponents();
    }

    void setNavigationView() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        setHeadersComponents(header);
    }

    void loadComponents() {
        if (userLogged != null) {
            nameUserLogged.setText(userLogged.getName());
            if(!tradesFragmentOn)
                setTradesFragment();
        }
    }

    void setHeadersComponents(View header) {
        if (userLogged != null) {
            nameUserLogged = (TextView) header.findViewById(R.id.nameUserLoged);
        }
    }

    void setMenu() {
        navigationView.inflateMenu(R.menu.main);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.sair:
                dialogSignOut();
                break;

            case R.id.matchs:
                setTradesFragment();
                break;

            case R.id.want_buy:
                setWantBuyTradesFragment();
                break;

            case R.id.want_sell:
                setWantSellTradesFragment();
                break;
        }
        item.setChecked(true);
        drawerLayout.closeDrawers();
        return true;
    }

    void setTradesFragment(){
        getSupportActionBar().setTitle(getString(R.string.matchs));
        switchFragment(new TradesFragment_());
        tradesFragmentOn = true;
    }

    void setWantBuyTradesFragment(){
        getSupportActionBar().setTitle(getString(R.string.want_buy));
        switchFragment(new WantBuyTradesFragment_());
        tradesFragmentOn = false;
    }

    void setWantSellTradesFragment(){
        getSupportActionBar().setTitle(getString(R.string.want_sell));
        switchFragment(new WantSellTradesFragment_());
        tradesFragmentOn = false;
    }

    @UiThread
    void dialogSignOut() {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(R.string.do_you_wish_go_out));
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        signOut();
                    }
                });
        builder.show();
    }

    @Background
    void signOut() {
        sessionService.signOut();
        callLoginActivity();
    }

    @UiThread
    void callLoginActivity() {
        LoginActivity_.intent(HomeActivity.this).start();
        finish();
    }

    @UiThread
    public void switchFragment(Fragment fragment) {
        if(currentFragment == fragment)
            return;
        currentFragment = fragment;
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        transaction.replace(R.id.home_container, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.press_again_to_go_out, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Receiver(actions = "otgmobile.com.br.tradingcar.UNAUTHORIZED")
    protected void checkUnauthorizedUser() {
        Toast.makeText(this, R.string.access_denied, Toast.LENGTH_SHORT).show();
        sessionService.clearTable();
        callLoginActivity();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

}
