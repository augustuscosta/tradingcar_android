package br.com.otgmobile.tradingcar.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

/**
 * Created by augustuscosta on 22/06/16.
 */
public class RestUtil {

    public static final String ROOT_URL_INDEX = "http://10.0.2.2:3000"; //local emulator
    public static final String ROOT_URL = "http://10.0.2.2:3000/api/";
    //public static final String ROOT_URL_INDEX = "http://192.168.0.19:3000"; // casa
    //public static final String ROOT_URL = "http://192.168.0.19:3000/api/";
    //public static final String ROOT_URL_INDEX = "http://tradingcarapp.com.br"; // producao
    //public static final String ROOT_URL = "http://tradingcarapp.com.br/api/";

    public static final String MOBILE_COOKIE = "_tradingcar_session";

    public static void loggerForError(RestClientException e){
        Log.e("ERRO NA CONEXÃO", "Erro ---> MENSAGEM: " + e.getMessage());
        Log.e("ERRO NA CONEXÃO", "Erro ---> CAUSA: " + e.getCause());
        Log.e("ERRO NA CONEXÃO", "Erro ---> MENSAGEM ESPECIFICA: " + e.getMostSpecificCause().getMessage());
    }

    public static void checkUnauthorized(RestClientException e, Context context){
        e.printStackTrace();
        try{
            HttpClientErrorException exception = (HttpClientErrorException) e;
            if(HttpStatus.UNAUTHORIZED.equals(exception.getStatusCode())){
                String action = "otgmobile.com.br.tradingcar.UNAUTHORIZED";
                Intent intent = new Intent();
                intent.setAction(action);
                context.sendBroadcast(intent);
            }
        }catch (Exception exception){
            exception.printStackTrace();
        }

    }
}
