package br.com.otgmobile.tradingcar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;
import java.util.Date;

/**
 * Created by augustuscosta on 03/05/17.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Trade {

    public static Integer SELL = 1;
    public static Integer BUY = 2;


    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private User user;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Store store;

    @DatabaseField
    @JsonProperty
    private Boolean active;

    @DatabaseField
    @JsonProperty("business_type_id")
    private Integer businessTypeId;

    @DatabaseField
    @JsonProperty
    private Boolean stock;

    @DatabaseField
    @JsonProperty
    private Float value;

    @DatabaseField
    @JsonProperty("value_to")
    private Float valueTo;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Vehicle vehicle;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Contact contact;

    @DatabaseField
    @JsonProperty("created_at")
    private Date createdAt;

    @DatabaseField
    @JsonProperty("updated_at")
    private Date updatedAt;

    @ForeignCollectionField
    @JsonProperty("matches")
    private Collection<Match> matches;

    @DatabaseField
    private Integer order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(Integer businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public Boolean getStock() {
        if(stock == null)
            stock = false;
        return stock;
    }

    public void setStock(Boolean stock) {
        this.stock = stock;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Collection<Match> getMatches() {
        return matches;
    }

    public void setMatches(Collection<Match> matches) {
        this.matches = matches;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Float getValueTo() {
        return valueTo;
    }

    public void setValueTo(Float valueTo) {
        this.valueTo = valueTo;
    }
}
