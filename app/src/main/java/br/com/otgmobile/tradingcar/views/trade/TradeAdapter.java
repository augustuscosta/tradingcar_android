package br.com.otgmobile.tradingcar.views.trade;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.Trade;
import br.com.otgmobile.tradingcar.model.VehicleImage;
import br.com.otgmobile.tradingcar.service.UserService;
import br.com.otgmobile.tradingcar.util.RestUtil;

/**
 * Created by augustuscosta on 02/05/17.
 */

public class TradeAdapter extends RecyclerView.Adapter<TradeAdapter.ViewHolder> {

    private List<Trade> trades = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private Context context;
    private UserService userService;
    private RecyclerView recycleView;

    private final View.OnClickListener clickListener = new TradClickListener();

    public TradeAdapter(Context context, List<Trade> trades, UserService userService, RecyclerView recycleView) {
        if(context != null) {
            this.context = context;
            this.trades = trades;
            this.userService = userService;
            this.recycleView = recycleView;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    public void setTrades(List<Trade> trades) {
        this.trades = trades;
    }

    public void addTrades(List<Trade> trades) {
        if(trades == null)
            trades = new ArrayList<>();
        this.trades.addAll(trades);
    }

    public List<Trade> getTrades() {
        return trades;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_trade, parent, false);
        view.setOnClickListener(clickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        configureUpdatedAt(holder, trades.get(position));
        configureIcons(holder, trades.get(position));
        configureData(holder, trades.get(position));
        configureImage(holder, trades.get(position));
    }

    private void configureUpdatedAt(ViewHolder holder, Trade trade){
       String date = DateFormat.getDateFormat(context).format(trade.getUpdatedAt());
        holder.updatedAtTextView.setText(date);
    }

    private void configureImage(ViewHolder holder, Trade trade) {
        if(trade.getVehicle().getVehicleImages() != null && trade.getVehicle().getVehicleImages().size() > 0){
            List<VehicleImage> images = new ArrayList<>(trade.getVehicle().getVehicleImages());
            Picasso.with(context).load(RestUtil.ROOT_URL_INDEX + images.get(0).getImage()).into(holder.imageView);
        }else {
             Picasso.with(context).load(RestUtil.ROOT_URL_INDEX + "/missing_car.png").into(holder.imageView);
        }
    }

    private void configureData(ViewHolder holder, Trade trade){
        configureMakeData(holder, trade);
        configureModelData(holder, trade);
        configureVersionData(holder, trade);
    }

    private void configureVersionData(ViewHolder holder, Trade trade) {
        if(trade.getVehicle().getVersion() == null){
            holder.versionContainer.setVisibility(View.GONE);
        }else {
            holder.versionTextView.setText(trade.getVehicle().getVersion().getName());
            holder.versionContainer.setVisibility(View.VISIBLE);
        }
    }

    private void configureModelData(ViewHolder holder, Trade trade) {
        if(trade.getVehicle().getModel() == null){
            holder.modelContainer.setVisibility(View.GONE);
        }else {
            holder.modelTextView.setText(trade.getVehicle().getModel().getName());
            holder.modelContainer.setVisibility(View.VISIBLE);
        }
    }

    private void configureMakeData(ViewHolder holder, Trade trade) {
        if(trade.getVehicle().getMake() == null){
            holder.makeContainer.setVisibility(View.GONE);
        }else {
            holder.makeTextView.setText(trade.getVehicle().getMake().getName());
            holder.makeContainer.setVisibility(View.VISIBLE);
        }
    }

    private void configureIcons(ViewHolder holder, Trade trade) {
        configureContactIcons(holder, trade);
        configureMatchIcons(holder, trade);
        configureBusinessType(holder, trade);
    }

    private void configureBusinessType(ViewHolder holder, Trade trade) {
        if(trade.getBusinessTypeId() == 1){
            holder.textViewBusinessTypeTextView.setText(context.getString(R.string.want_sell));
            holder.addShoppingCartIcon.setVisibility(View.GONE);
            holder.removeShoppingCartIcon.setVisibility(View.VISIBLE);
        }else{
            holder.textViewBusinessTypeTextView.setText(context.getString(R.string.want_buy));
            holder.addShoppingCartIcon.setVisibility(View.VISIBLE);
            holder.removeShoppingCartIcon.setVisibility(View.GONE);
        }
    }

    private void configureMatchIcons(ViewHolder holder, Trade trade) {
        if(trade.getMatches() != null && trade.getMatches().size() > 0){
            holder.matchIcon.setVisibility(View.VISIBLE);
        }else {
            holder.matchIcon.setVisibility(View.GONE);
        }
    }

    private void configureContactIcons(ViewHolder holder, Trade trade) {

        if(trade.getStock() == true){
            holder.homeIcon.setVisibility(View.VISIBLE);
            holder.contactIcon.setVisibility(View.GONE);
            holder.otherUserIcon.setVisibility(View.GONE);
            return;
        }

        if(trade.getUser().getId() == userService.getLocalCurrentUser().getId()){
            holder.homeIcon.setVisibility(View.GONE);
            holder.contactIcon.setVisibility(View.VISIBLE);
            holder.otherUserIcon.setVisibility(View.GONE);
        }else {
            holder.homeIcon.setVisibility(View.GONE);
            holder.contactIcon.setVisibility(View.GONE);
            holder.otherUserIcon.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public int getItemCount() {
        return trades.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView matchIcon;
        ImageView homeIcon;
        ImageView contactIcon;
        ImageView otherUserIcon;
        ImageView addShoppingCartIcon;
        ImageView removeShoppingCartIcon;

        LinearLayout makeContainer;
        LinearLayout modelContainer;
        LinearLayout versionContainer;

        TextView makeTextView;
        TextView modelTextView;
        TextView versionTextView;
        TextView textViewBusinessTypeTextView;
        TextView updatedAtTextView;

        ImageView imageView;


        ViewHolder(final View itemView) {
            super(itemView);

            matchIcon = (ImageView) itemView.findViewById(R.id.matchIcon);
            homeIcon = (ImageView) itemView.findViewById(R.id.homeIcon);
            contactIcon = (ImageView) itemView.findViewById(R.id.contactIcon);
            otherUserIcon = (ImageView) itemView.findViewById(R.id.otherUserIcon);
            addShoppingCartIcon = (ImageView) itemView.findViewById(R.id.addShoppingCartIcon);
            removeShoppingCartIcon = (ImageView) itemView.findViewById(R.id.removeShoppingCartIcon);

            makeTextView = (TextView) itemView.findViewById(R.id.makeTextView);
            modelTextView = (TextView) itemView.findViewById(R.id.modelTextView);
            versionTextView = (TextView) itemView.findViewById(R.id.versionTextView);
            textViewBusinessTypeTextView = (TextView) itemView.findViewById(R.id.textViewBusinessTypeTextView);
            updatedAtTextView = (TextView) itemView.findViewById(R.id.updatedAtTextView);

            makeContainer = (LinearLayout) itemView.findViewById(R.id.makeContainer);
            modelContainer = (LinearLayout) itemView.findViewById(R.id.modelContainer);
            versionContainer = (LinearLayout) itemView.findViewById(R.id.versionContainer);

            imageView = (ImageView) itemView.findViewById(R.id.imageView);

        }
    }

    private class TradClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            int position = recycleView.getChildAdapterPosition(view);
            ShowTradeActivity_.intent(context).tradeId(trades.get(position).getId()).start();
        }
    }
}
