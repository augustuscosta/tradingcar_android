package br.com.otgmobile.tradingcar.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import br.com.otgmobile.tradingcar.R;


/**
 * Created by augustuscosta on 12/07/16.
 */
public class DialogUtil {


    private static DialogInterface.OnClickListener getNegativeButton() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
    }

    public static AlertDialog.Builder createDialog(Context context,String message) {
        return new AlertDialog.Builder(context, R.style.ThemeOverlay_AppCompat_Dialog)
                .setCancelable(false)
                .setMessage(message);
    }

    public static AlertDialog.Builder getNegativeButton(AlertDialog.Builder builder){
        builder.setNegativeButton(R.string.cancel, getNegativeButton());
        return builder;
    }

    public static AlertDialog.Builder createDialog(Context context,String message, String title) {
        return new AlertDialog.Builder(context, R.style.ThemeOverlay_AppCompat_Dialog)
                .setCancelable(true)
                .setMessage(message)
                .setTitle(title)
                .setNeutralButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
    }

}
