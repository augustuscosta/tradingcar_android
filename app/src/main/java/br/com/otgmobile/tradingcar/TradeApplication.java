package br.com.otgmobile.tradingcar;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by augustuscosta on 09/05/17.
 */

public class TradeApplication extends Application {

    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }


}
