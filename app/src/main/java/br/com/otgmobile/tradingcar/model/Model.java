package br.com.otgmobile.tradingcar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by augustuscosta on 24/04/17.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Model {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty("id_fipe")
    private Integer idFipe;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty("fipe_name")
    private String fipeName;

    @DatabaseField
    @JsonProperty
    private String marca;


    @DatabaseField
    @JsonProperty
    private String key;

    @DatabaseField
    @JsonProperty("fipe_marca")
    private String fipeMarca;


    @DatabaseField
    @JsonProperty("make_id")
    private Integer makeId;

    @ForeignCollectionField
    @JsonProperty
    private Collection<Vehicle> vehicles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFipe() {
        return idFipe;
    }

    public void setIdFipe(Integer idFipe) {
        this.idFipe = idFipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFipeName() {
        return fipeName;
    }

    public void setFipeName(String fipeName) {
        this.fipeName = fipeName;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFipeMarca() {
        return fipeMarca;
    }

    public void setFipeMarca(String fipeMarca) {
        this.fipeMarca = fipeMarca;
    }

    public Integer getMakeId() {
        return makeId;
    }

    public void setMakeId(Integer makeId) {
        this.makeId = makeId;
    }

    public Collection<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
