package br.com.otgmobile.tradingcar.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.List;

import br.com.otgmobile.tradingcar.cloud.SessionRest;
import br.com.otgmobile.tradingcar.model.Session;
import br.com.otgmobile.tradingcar.model.User;
import br.com.otgmobile.tradingcar.util.DatabaseHelper;
import br.com.otgmobile.tradingcar.util.RestUtil;

/**
 * Created by augustuscosta on 23/06/16.
 */
@EBean
public class SessionService{

    @RestService
    SessionRest sessionRest;

    @RootContext
    Context context;

    @Bean(CookieService.class)
    CookieService cookieUtils;

    @Bean
    TradeService tradeService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Session, Integer> sessionDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<User, Integer> userDao;


    public boolean signIn(Session session) {
        boolean returnValue;
        try {

            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            persist(sessionRest.signIn(session));
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));

            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }


    public boolean signOut() {

        try {
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            sessionRest.signOut();
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));
            clearTable();

            return true;

        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            return false;
        }

    }

    public boolean resetPassword(Session session) {
        boolean returnValue;
        try {
            sessionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            sessionRest.resetPassword(session);
            cookieUtils.storedCookie(sessionRest.getCookie(RestUtil.MOBILE_COOKIE));

            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }

    private void persist(Session session){
        try{
            sessionDao.createOrUpdate(session);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }


    public Session getCurrentSession() {
        try {
            List<Session> sessions =  sessionDao.queryForAll();
            if(sessions.size() > 0)
                return sessions.get(0);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void clearTable() {
        try {
            TableUtils.clearTable(sessionDao.getConnectionSource(), Session.class);
            TableUtils.clearTable(userDao.getConnectionSource(), User.class);
            tradeService.clearTable();
            //TODO Add all tables her

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
