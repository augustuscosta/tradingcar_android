package br.com.otgmobile.tradingcar.views.trade;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.Trade;
import br.com.otgmobile.tradingcar.model.VehicleImage;
import br.com.otgmobile.tradingcar.service.TradeService;
import br.com.otgmobile.tradingcar.service.UserService;
import br.com.otgmobile.tradingcar.util.DialogUtil;
import br.com.otgmobile.tradingcar.util.ProgressDialogUtil;
import br.com.otgmobile.tradingcar.util.RestUtil;
import br.com.otgmobile.tradingcar.util.StringUtil;

@EActivity(R.layout.activity_show_trade)
public class ShowTradeActivity extends AppCompatActivity {

    @Bean
    TradeService tradeService;

    @ViewById
    RecyclerView recyclerViewTrades;

    @Bean
    UserService userService;

    @ViewById
    Toolbar toolbar;

    @Extra
    Integer tradeId;

    @ViewById
    ImageView buyImage;

    @ViewById
    ImageView sellImage;

    @ViewById
    ImageView stockImage;

    @ViewById
    TextView textViewBusinessType;

    @ViewById
    CardView actionContainer;

    @ViewById
    CardView vehicleContainer;

    @ViewById
    LinearLayout makeContainer;

    @ViewById
    TextView textViewMake;

    @ViewById
    LinearLayout modelContainer;

    @ViewById
    TextView textViewModel;

    @ViewById
    LinearLayout versionContainer;

    @ViewById
    TextView textViewVersion;

    @ViewById
    LinearLayout yearContainer;

    @ViewById
    TextView textViewYear;

    @ViewById
    LinearLayout yearFromContainer;

    @ViewById
    TextView textViewYearFrom;

    @ViewById
    LinearLayout yearToContainer;

    @ViewById
    TextView textViewYearTo;

    @ViewById
    LinearLayout fuelContainer;

    @ViewById
    TextView textViewFuel;

    @ViewById
    LinearLayout doorsContainer;

    @ViewById
    TextView textViewDoors;

    @ViewById
    LinearLayout kmContainer;

    @ViewById
    TextView textViewKm;

    @ViewById
    LinearLayout kmFromContainer;

    @ViewById
    TextView textViewKmFrom;

    @ViewById
    LinearLayout kmToContainer;

    @ViewById
    TextView textViewKmTo;

    @ViewById
    LinearLayout colorContainer;

    @ViewById
    TextView textViewColor;

    @ViewById
    LinearLayout gearBoxContainer;

    @ViewById
    TextView textViewGearBox;

    @ViewById
    LinearLayout carBodyContainer;

    @ViewById
    TextView textViewCarBody;

    @ViewById
    LinearLayout armoredContainer;

    @ViewById
    LinearLayout detailsContainer;

    @ViewById
    TextView textViewDetails;

    @ViewById
    CardView contactContainer;

    @ViewById
    LinearLayout nameContainer;

    @ViewById
    TextView textViewName;

    @ViewById
    LinearLayout phoneContainer;

    @ViewById
    TextView textViewPhone;

    @ViewById
    LinearLayout emailContainer;

    @ViewById
    TextView textViewEmail;

    @ViewById
    ImageButton contactIcon;

    @ViewById
    ImageButton otherUserIcon;

    @ViewById
    CardView valueContainer;

    @ViewById
    LinearLayout valueFieldContainer;

    @ViewById
    TextView textViewValue;

    @ViewById
    CardView valueFromContainer;

    @ViewById
    LinearLayout valueFromFieldContainer;

    @ViewById
    TextView textViewValueFrom;

    @ViewById
    CardView valueToContainer;

    @ViewById
    LinearLayout valueToFieldContainer;

    @ViewById
    TextView textViewValueTo;

    @ViewById
    CardView matchesContainer;

    @ViewById
    ImageView vehicleImageView;

    private ProgressDialog progress;

    private Trade trade;
    private List<Trade> trades;

    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadComponents();
    }

    @Background()
    void loadComponents() {
        startProgressDialod();
        try {
            trade = tradeService.getCloud(tradeId);
            setTradeDataToLayout();
            trades = tradeService.getMatches(tradeId);
            setMatchDataToLayout();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        closeProgressDialod();
    }

    @UiThread
    void setMatchDataToLayout() {
        if (trades == null || trades.size() == 0) {
            return;
        }
        TradeAdapter tradeAdapter = new TradeAdapter(this, trades, userService, recyclerViewTrades);
        recyclerViewTrades.setAdapter(tradeAdapter);
        recyclerViewTrades.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        tradeAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @UiThread
    void startProgressDialod() {
        progress = ProgressDialogUtil.callProgressDialog(this, getString(R.string.loading));
    }

    @UiThread
    void closeProgressDialod() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }


    @Click(R.id.emailButton)
    void emailButtonClick() {
        final Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{trade.getContact().getEmail()});
        startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
    }

    @Click(R.id.phoneButton)
    void phoneButtonClick() {
        Uri call = Uri.parse("tel:" + trade.getContact().getPhone());
        Intent calIntent = new Intent(Intent.ACTION_DIAL, call);
        startActivity(calIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.show_trade, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                deleteButton();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void deleteButton() {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(R.string.do_you_wish_delete_trade));
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deleteTrade();
                    }
                });
        builder.show();
    }

    @Background
    void deleteTrade() {
        startProgressDialod();
        try {
            trade = tradeService.delete(tradeId);
            closeProgressDialod();
            deleteSucess();
        } catch (Exception e1) {
            e1.printStackTrace();
            closeProgressDialod();
        }
    }

    @UiThread
    void deleteSucess() {
        Toast.makeText(this, getString(R.string.trade_delete_sucess), Toast.LENGTH_SHORT).show();
        finish();
    }

    @UiThread
    void setTradeDataToLayout() {
        if (trade == null)
            return;
        addBusinessTypeData();
        addVehicleData();
        addStockData();
        addContactData();
        addValueData();
        addMatchesData();
    }

    private void addMatchesData() {
        if (trade.getMatches() == null || trade.getMatches().isEmpty())
            return;

        matchesContainer.setVisibility(View.VISIBLE);
    }

    private void addValueData() {
        if(trade.getBusinessTypeId() == Trade.BUY){
            if (trade.getValue() != null)
                valueFromContainer.setVisibility(View.VISIBLE);
            if (trade.getValueTo() != null)
                valueToContainer.setVisibility(View.VISIBLE);
        }else{
            if (trade.getValue() != null)
                valueContainer.setVisibility(View.VISIBLE);
        }

    }

    @LongClick({R.id.valueButton})
    void valueButtonLongClick() {
        addValueFieldData();
    }

    @Click(R.id.valueButton)
    void valueButtonClick() {
        hideValueFieldData();
    }

    private void addValueFieldData() {
        textViewValue.setVisibility(View.VISIBLE);
        textViewValue.setText(StringUtil.getStringValueFromCurrencyFloat(trade.getValue()));
    }

    private void hideValueFieldData() {
        textViewValue.setVisibility(View.GONE);
    }

    @LongClick({R.id.valueFromButton})
    void valueFromButtonLongClick() {
        addValueFromFieldData();
    }

    @Click(R.id.valueFromButton)
    void valueFromButtonClick() {
        hideValueFromFieldData();
    }

    private void addValueFromFieldData() {
        textViewValueFrom.setVisibility(View.VISIBLE);
        textViewValueFrom.setText(getString(R.string.value_from) + " " + StringUtil.getStringValueFromCurrencyFloat(trade.getValue()));
    }

    private void hideValueFromFieldData() {
        textViewValueFrom.setVisibility(View.GONE);
    }

    @LongClick({R.id.valueToButton})
    void valueToButtonLongClick() {
        addValueToFieldData();
    }

    @Click(R.id.valueToButton)
    void valueToButtonClick() {
        hideValueToFieldData();
    }

    private void addValueToFieldData() {
        textViewValueTo.setVisibility(View.VISIBLE);
        textViewValueTo.setText(getString(R.string.value_to) + " " + StringUtil.getStringValueFromCurrencyFloat(trade.getValueTo()));
    }

    private void hideValueToFieldData() {
        textViewValueTo.setVisibility(View.GONE);
    }

    private void addContactData() {
        if (true == trade.getStock() || trade.getContact() == null)
            return;
        contactContainer.setVisibility(View.VISIBLE);
        nameContainer.setVisibility(View.VISIBLE);
        showContactButtonIcon();

    }

    private void showContactButtonIcon() {
        if (userService.getLocalCurrentUser().getId() == trade.getUser().getId()) {
            contactIcon.setVisibility(View.VISIBLE);
            otherUserIcon.setVisibility(View.GONE);
        } else {
            contactIcon.setVisibility(View.GONE);
            otherUserIcon.setVisibility(View.VISIBLE);
        }
    }

    @LongClick({R.id.contactIcon, R.id.otherUserIcon})
    void contactLongButtonClick() {
        addNameData();
        addPhoneData();
        addEmailData();
    }

    @Click({R.id.contactIcon, R.id.otherUserIcon})
    void contacButtonClick() {
        hideNameData();
        hidePhoneData();
        hideEmailData();
    }

    private void hideNameData() {
        textViewName.setVisibility(View.GONE);
    }

    private void hidePhoneData() {
        phoneContainer.setVisibility(View.GONE);
    }

    private void hideEmailData() {
        emailContainer.setVisibility(View.GONE);
    }

    private void addNameData() {
        String value = null;

        if (userService.getLocalCurrentUser().getId() == trade.getUser().getId()) {
            value = trade.getContact().getName();
        } else {
            if (userService.getLocalCurrentUser().getUserTypeId() == 2) {
                value = trade.getContact().getName();
            } else {
                value = trade.getUser().getName();
            }
        }

        if (value == null || value.isEmpty())
            return;

        textViewName.setText(value);
        textViewName.setVisibility(View.VISIBLE);
    }

    private void addPhoneData() {
        String value = null;

        if (userService.getLocalCurrentUser().getId() == trade.getUser().getId()) {
            value = trade.getContact().getPhone();
        } else {
            if (userService.getLocalCurrentUser().getUserTypeId() == 2) {
                value = trade.getContact().getPhone();
            } else {
                value = trade.getUser().getPhone();
            }
        }

        if (value == null || value.isEmpty())
            return;
        phoneContainer.setVisibility(View.VISIBLE);
        textViewPhone.setText(value);
    }

    private void addEmailData() {

        String value = null;

        if (userService.getLocalCurrentUser().getId() == trade.getUser().getId()) {
            value = trade.getContact().getEmail();
        } else {
            if (userService.getLocalCurrentUser().getUserTypeId() == 2) {
                value = trade.getContact().getEmail();
            } else {
                value = trade.getUser().getEmail();
            }
        }

        if (value == null || value.isEmpty())
            return;
        emailContainer.setVisibility(View.VISIBLE);
        textViewEmail.setText(value);
    }

    private void addStockData() {
        if (!true == trade.getStock())
            return;
        stockImage.setVisibility(View.VISIBLE);
    }

    private void addVehicleData() {
        vehicleContainer.setVisibility(View.VISIBLE);
        addMakeData();
        addModelData();
        addVersionData();
        addYearData();
        addFuelData();
        addDoorsData();
        addKmData();
        addColorData();
        addGearBoxData();
        addCarBodyData();
        addArmoreData();
        addDetailsData();
        addVehicleImage();
    }

    private void addVehicleImage(){
        if(trade.getVehicle().getVehicleImages() == null || trade.getVehicle().getVehicleImages().isEmpty())
            return;

        ViewGroup.LayoutParams params = vehicleImageView.getLayoutParams();
        params.height = 1200;
        vehicleImageView.setLayoutParams(params);
        List<VehicleImage> images = new ArrayList<>(trade.getVehicle().getVehicleImages());
        Picasso.with(this).load(RestUtil.ROOT_URL_INDEX + images.get(0).getImage()).into(vehicleImageView);
        toolbar.setBackgroundColor(00000000); // i don't tested this method. Write if it's not working
        toolbar.setTitleTextColor(00000000);
    }

    @Click(R.id.vehicleImageView)
    void imageClick(){
        List<String> list = new ArrayList<>();

        for(VehicleImage image: trade.getVehicle().getVehicleImages())
            list.add(RestUtil.ROOT_URL_INDEX + image.getImage());

        new ImageViewer.Builder(this, list)
                .setStartPosition(0)
                .show();
    }

    private void addMakeData() {
        if (trade.getVehicle().getMake() == null)
            return;
        makeContainer.setVisibility(View.VISIBLE);
        textViewMake.setText(trade.getVehicle().getMake().getName());
    }

    private void addModelData() {
        if (trade.getVehicle().getModel() == null)
            return;
        modelContainer.setVisibility(View.VISIBLE);
        textViewModel.setText(trade.getVehicle().getModel().getName());
    }

    private void addVersionData() {
        if (trade.getVehicle().getVersion() == null)
            return;
        versionContainer.setVisibility(View.VISIBLE);
        textViewVersion.setText(trade.getVehicle().getVersion().getName());
    }

    private void addYearData() {
        if(trade.getBusinessTypeId() == Trade.BUY){
            if (trade.getVehicle().getYear() != null && !trade.getVehicle().getYear().isEmpty()){
                textViewYearFrom.setText( getString(R.string.year_from) + " " + trade.getVehicle().getYear());
                yearFromContainer.setVisibility(View.VISIBLE);
            }

            if (trade.getVehicle().getYearTo() != null && !trade.getVehicle().getYearTo().isEmpty()){
                textViewYearTo.setText( getString(R.string.year_to) + " " + trade.getVehicle().getYearTo());
                yearToContainer.setVisibility(View.VISIBLE);
            }
        }else{
            if (trade.getVehicle().getYear() != null && !trade.getVehicle().getYear().isEmpty()){
                textViewYear.setText(trade.getVehicle().getYear());
                yearContainer.setVisibility(View.VISIBLE);
            }
        }

    }

    private void addFuelData() {
        if (trade.getVehicle().getFuel() == null || trade.getVehicle().getFuel().isEmpty())
            return;
        fuelContainer.setVisibility(View.VISIBLE);
        textViewFuel.setText(trade.getVehicle().getFuel());
    }

    private void addDoorsData() {
        if (trade.getVehicle().getDoors() == null || trade.getVehicle().getDoors().isEmpty())
            return;
        doorsContainer.setVisibility(View.VISIBLE);
        textViewDoors.setText(trade.getVehicle().getDoors());
    }

    private void addKmData() {
        if(trade.getBusinessTypeId() == Trade.BUY){
            if (trade.getVehicle().getKm() != null && !trade.getVehicle().getKm().isEmpty()){
                textViewKmFrom.setText(getString(R.string.km_from) + " " +StringUtil.getStringValueFromKMString(trade.getVehicle().getKm()));
                kmFromContainer.setVisibility(View.VISIBLE);
            }
            if (trade.getVehicle().getKmTo() != null && !trade.getVehicle().getKmTo().isEmpty()){
                textViewKmTo.setText(getString(R.string.km_to) + " " +StringUtil.getStringValueFromKMString(trade.getVehicle().getKmTo()));
                kmToContainer.setVisibility(View.VISIBLE);
            }
        }else{
            if (trade.getVehicle().getKm() != null && !trade.getVehicle().getKm().isEmpty()){
                textViewKm.setText(StringUtil.getStringValueFromKMString(trade.getVehicle().getKm()));
                kmContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    private void addColorData() {
        if (trade.getVehicle().getColor() == null || trade.getVehicle().getColor().isEmpty())
            return;
        colorContainer.setVisibility(View.VISIBLE);
        textViewColor.setText(trade.getVehicle().getColor());
    }

    private void addGearBoxData() {
        if (trade.getVehicle().getGearBox() == null || trade.getVehicle().getGearBox().isEmpty())
            return;
        gearBoxContainer.setVisibility(View.VISIBLE);
        textViewGearBox.setText(trade.getVehicle().getGearBox());
    }

    private void addCarBodyData() {
        if (trade.getVehicle().getCarBody() == null || trade.getVehicle().getCarBody().isEmpty())
            return;
        carBodyContainer.setVisibility(View.VISIBLE);
        textViewCarBody.setText(trade.getVehicle().getCarBody());
    }

    private void addArmoreData() {
        if (trade.getVehicle().getArmored() == null || !trade.getVehicle().getArmored())
            return;
        armoredContainer.setVisibility(View.VISIBLE);
    }

    private void addDetailsData() {
        if (trade.getVehicle().getDetails() == null || trade.getVehicle().getDetails().isEmpty())
            return;
        detailsContainer.setVisibility(View.VISIBLE);
        textViewDetails.setText(trade.getVehicle().getDetails());
    }

    private void addBusinessTypeData() {
        textViewBusinessType.setVisibility(View.VISIBLE);
        actionContainer.setVisibility(View.VISIBLE);
        if (Trade.BUY.equals(trade.getBusinessTypeId())) {
            buyImage.setVisibility(View.VISIBLE);
            sellImage.setVisibility(View.GONE);
            textViewBusinessType.setText(getString(R.string.want_buy));
        } else {
            buyImage.setVisibility(View.GONE);
            sellImage.setVisibility(View.VISIBLE);
            textViewBusinessType.setText(getString(R.string.want_sell));
        }
    }
}
