package br.com.otgmobile.tradingcar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;


/**
 * Created by augustuscosta on 04/07/16.
 */
@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class User{

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty
    private String email;

    @DatabaseField
    @JsonProperty
    private String phone;

    @DatabaseField
    @JsonProperty("user_type_id")
    private Integer userTypeId;

    @DatabaseField
    @JsonProperty
    private String password;

    @DatabaseField
    @JsonProperty("password_confirmation")
    private String passwordConfirmation;

    @DatabaseField
    @JsonProperty("current_user")
    private Boolean currentUser;

    @ForeignCollectionField
    @JsonProperty
    private Collection<Trade> trades;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public Boolean getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(Boolean currentUser) {
        this.currentUser = currentUser;
    }

    public Collection<Trade> getTrades() {
        return trades;
    }

    public void setTrades(Collection<Trade> trades) {
        this.trades = trades;
    }
}
