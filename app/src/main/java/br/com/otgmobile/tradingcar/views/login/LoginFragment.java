package br.com.otgmobile.tradingcar.views.login;


import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.Session;
import br.com.otgmobile.tradingcar.service.DeviceIdService;
import br.com.otgmobile.tradingcar.service.SessionService;
import br.com.otgmobile.tradingcar.util.ProgressDialogUtil;
import br.com.otgmobile.tradingcar.util.ValidateUtil;


/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_login)
public class LoginFragment extends Fragment{

    @ViewById(R.id.editTextEmail)
    EditText email;

    @ViewById(R.id.editTextPassword)
    EditText password;

    @Bean
    SessionService sessionService;

    @Bean
    DeviceIdService deviceIdService;

    private ProgressDialog progress;

    private LoginActivity loginActivity;

    public void setLoginActivity(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
    }

    @Click(R.id.buttonLogin)
    void loginButton() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        checkedList.add(ValidateUtil.checkInterval(password, 6, 18, getString(R.string.invalid_password)));

        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            login();
        }
    }


    @Background
    void login() {
        startProgressDialod();

        Session session = new Session();
        session.setEmail(email.getText().toString());
        session.setPassword(password.getText().toString());

        if (sessionService.signIn(session)) {
            deviceIdService.sendDeviceId();
            closeProgressDialod();
            callHomeActivity();

        } else {
            closeProgressDialod();
            callToastWrongEmailOrPassword();
        }
    }

    @UiThread
    void startProgressDialod() {
        progress = ProgressDialogUtil.callProgressDialog(getActivity(), getString(R.string.check_login_data));
    }

    @UiThread
    void closeProgressDialod() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

    @UiThread
    void callToastWrongEmailOrPassword() {
        Toast.makeText(getActivity(), R.string.email_or_password_incorrect, Toast.LENGTH_LONG).show();
    }

    @Click(R.id.textViewForgotPassword)
    void goToRecoverPassword() {
        loginActivity.goToRecoverPassword();
    }

    @UiThread
    void callHomeActivity() {
        loginActivity.callHomeActivity();
    }

}



