package br.com.otgmobile.tradingcar.service;

import android.content.Context;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;

import br.com.otgmobile.tradingcar.cloud.VehicleImageRest;
import br.com.otgmobile.tradingcar.model.Vehicle;
import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class VehicleImageService {

    @RootContext
    Context context;

    @Bean
    CookieService cookieService;

    @RestService
    VehicleImageRest vehicleImageRest;

    public void upload(Vehicle vehicle, FileSystemResource image) {
        MultiValueMap<String, Object> data = new LinkedMultiValueMap<>();
        HttpHeaders imageHeaders = new HttpHeaders();
        imageHeaders.setContentType(MediaType.IMAGE_PNG);

        if (image != null) {
            HttpEntity<Resource> imageEntity = new HttpEntity(image, imageHeaders);
            addValue(data, "vehicle_image[image]", imageEntity);
        }

        addValueAsString(data, "vehicle_image[vehicle_id]", vehicle.getId());


        try {
            vehicleImageRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            vehicleImageRest.setHeader("Content-Type", MediaType.MULTIPART_FORM_DATA_VALUE);
            vehicleImageRest.uploadImage(data);
            cookieService.storedCookie(vehicleImageRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }

    }

    private void addValue(MultiValueMap data, String key, Object value) {
        if (value == null) {
            return;
        } else {
            data.set(key, value);
        }
    }

    private void addValueAsString(MultiValueMap data, String key, Object value) {
        if (value == null) {
            return;
        } else {
            data.set(key, value + "");
        }
    }
}
