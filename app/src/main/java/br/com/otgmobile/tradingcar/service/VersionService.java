package br.com.otgmobile.tradingcar.service;

import android.content.Context;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.List;

import br.com.otgmobile.tradingcar.cloud.VersionRest;
import br.com.otgmobile.tradingcar.model.Model;
import br.com.otgmobile.tradingcar.model.Version;
import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class VersionService {

    @RootContext
    Context context;

    @RestService
    VersionRest versionRest;

    @Bean
    CookieService cookieService;

    public List<Version> searchVersions(String search, Integer makeId, Integer modelId){
        versionRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
        return versionRest.searchVersions(search, makeId, modelId);
    }

}
