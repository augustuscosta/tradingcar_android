package br.com.otgmobile.tradingcar.service;

import android.content.Context;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.List;

import br.com.otgmobile.tradingcar.cloud.ModelRest;
import br.com.otgmobile.tradingcar.model.Model;
import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class ModelService {

    @RootContext
    Context context;

    @RestService
    ModelRest modelRest;

    @Bean
    CookieService cookieService;

    public List<Model> searchModels(String search, Integer makeId){
        modelRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
        return modelRest.searchModels(search, makeId);
    }

}
