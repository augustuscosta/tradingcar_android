package br.com.otgmobile.tradingcar.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmobile.tradingcar.cloud.MakeRest;
import br.com.otgmobile.tradingcar.cloud.UserRest;
import br.com.otgmobile.tradingcar.model.Make;
import br.com.otgmobile.tradingcar.model.User;
import br.com.otgmobile.tradingcar.util.DatabaseHelper;
import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class MakeService {

    @RootContext
    Context context;

    @RestService
    MakeRest makeRest;

    @Bean
    CookieService cookieService;

    public List<Make> searchMakes(String search){
        makeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
        return makeRest.searchMakes(search);
    }

}
