package br.com.otgmobile.tradingcar.views.trade;


import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.Trade;
import br.com.otgmobile.tradingcar.service.SearchService;
import br.com.otgmobile.tradingcar.service.TradeService;
import br.com.otgmobile.tradingcar.service.UserService;
import br.com.otgmobile.tradingcar.util.ProgressDialogUtil;

@EFragment(R.layout.fragment_trades)
public class WantSellTradesFragment extends Fragment {

    @ViewById
    RecyclerView recyclerViewTrades;

    @ViewById
    TextView emptyMessageTextView;

    @ViewById(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @Bean
    TradeService tradeService;

    @Bean
    UserService userService;

    @Bean
    SearchService searchService;

    private ProgressDialog progress;

    private TradeAdapter tradeAdapter;

    @Click(R.id.new_trade_button)
    void newTradeButtonClick() {
        NewTradeActivity_.intent(this).start();
    }


    @Override
    public void onResume() {
        super.onResume();
        showEmptyMessage();
        loadComponents();
        listenRefresh();

        recyclerViewTrades.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(-1)) {
                    onScrolledToTop();
                } else if (!recyclerView.canScrollVertically(1)) {
                    onScrolledToBottom();
                } else if (dy < 0) {
                    onScrolledUp();
                } else if (dy > 0) {
                    onScrolledDown();
                }
            }

            public void onScrolledUp() {}

            public void onScrolledDown() {}

            public void onScrolledToTop() {}

            public void onScrolledToBottom() {
                if ("".equals(searchService.acquireSearch())){
                    if(tradeAdapter != null && tradeAdapter.getTrades() != null)
                        loadComponents(tradeAdapter.getTrades().size());
                }
            }
        });
    }

    private void listenRefresh() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadComponents();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Background()
    void loadComponents() {
        if(!"".equals(searchService.acquireSearch()))
            return;
        startProgressDialod();
        try {
           setAdapterRecyclerView(tradeService.getCurrentUserSellTrades());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        closeProgressDialod();
    }

    @Background()
    void loadComponents(int size) {
        startProgressDialod();
        try {
            List<Trade> trades = tradeService.getCurrentUserSellTrades(size);
            notifyDataChange(trades);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        closeProgressDialod();
    }

    @UiThread
    void notifyDataChange(List<Trade> trades) {

        tradeAdapter.addTrades(trades);
        tradeAdapter.notifyDataSetChanged();
    }

    @UiThread
    void setAdapterRecyclerView(List<Trade> trades) {

        if(trades.size() == 0){
            showEmptyMessage();
            return;
        }

        recyclerViewTrades.setVisibility(View.VISIBLE);
        emptyMessageTextView.setVisibility(View.GONE);

        tradeAdapter = new TradeAdapter(getActivity(), trades, userService, recyclerViewTrades);
        recyclerViewTrades.setAdapter(tradeAdapter);
        recyclerViewTrades.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        tradeAdapter.notifyDataSetChanged();
    }

    private void showEmptyMessage(){
        recyclerViewTrades.setVisibility(View.GONE);
        emptyMessageTextView.setVisibility(View.VISIBLE);
    }

    @UiThread
    void startProgressDialod() {
        progress = ProgressDialogUtil.callProgressDialog(getActivity(), getString(R.string.loading));
    }

    @UiThread
    void closeProgressDialod() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }



}
