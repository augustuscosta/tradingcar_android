package br.com.otgmobile.tradingcar.views.login;


import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.Session;
import br.com.otgmobile.tradingcar.service.SessionService;
import br.com.otgmobile.tradingcar.util.ProgressDialogUtil;
import br.com.otgmobile.tradingcar.util.ValidateUtil;


/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_recover_passowrd)
public class RecoverPasswordFragment extends Fragment {

    @ViewById(R.id.editTextSendEmail)
    EditText email;

    @Bean
    SessionService sessionService;

    private ProgressDialog progress;

    private LoginActivity loginActivity;

    public void setLoginActivity(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    @Click(R.id.buttonSendEmail)
    void resetPasswordButton() {
        List<Boolean> checkedList = new ArrayList<>();
        checkedList.add(ValidateUtil.isNotNullEmail(email, getString(R.string.invalid_email)));
        if (ValidateUtil.checkIfAllAreTrue(checkedList)) {
            resetPassword();
        }
    }

    @Background
    void resetPassword() {
        startProgressDialod();
        boolean send = sessionService.resetPassword(getSession());
        closeProgressDialod();
        if (send)
            showResetPasswordToast();
        else
            emailNotFound();
    }

    private Session getSession() {
        Session session;
        session = new Session();
        session.setEmail(email.getText().toString());
        return session;
    }

    @UiThread
    void showResetPasswordToast() {
        Toast.makeText(getActivity(), R.string.msg_to_recover_password, Toast.LENGTH_LONG).show();
    }

    @UiThread
    void emailNotFound() {
        Toast.makeText(getActivity(), R.string.email_not_found, Toast.LENGTH_LONG).show();
    }

    @UiThread
    void startProgressDialod() {
        progress = ProgressDialogUtil.callProgressDialog(getActivity(), getString(R.string.sending));
    }

    @UiThread
    void closeProgressDialod() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

}
