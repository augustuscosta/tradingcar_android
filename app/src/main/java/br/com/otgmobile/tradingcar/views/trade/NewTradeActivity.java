package br.com.otgmobile.tradingcar.views.trade;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Touch;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.Contact;
import br.com.otgmobile.tradingcar.model.Make;
import br.com.otgmobile.tradingcar.model.Model;
import br.com.otgmobile.tradingcar.model.Trade;
import br.com.otgmobile.tradingcar.model.User;
import br.com.otgmobile.tradingcar.model.Vehicle;
import br.com.otgmobile.tradingcar.model.Version;
import br.com.otgmobile.tradingcar.service.ImageService;
import br.com.otgmobile.tradingcar.service.MakeService;
import br.com.otgmobile.tradingcar.service.ModelService;
import br.com.otgmobile.tradingcar.service.TradeService;
import br.com.otgmobile.tradingcar.service.UserService;
import br.com.otgmobile.tradingcar.service.VehicleImageService;
import br.com.otgmobile.tradingcar.service.VersionService;
import br.com.otgmobile.tradingcar.util.DialogUtil;
import br.com.otgmobile.tradingcar.util.EditTextUtil;
import br.com.otgmobile.tradingcar.util.ProgressDialogUtil;
import br.com.otgmobile.tradingcar.util.StringUtil;
import br.com.otgmobile.tradingcar.util.ValidateUtil;

@EActivity(R.layout.activity_new_trade)
public class NewTradeActivity extends AppCompatActivity implements ImageRecycleViewNotifier {

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @Bean
    UserService userService;

    @Bean
    ImageService imageService;

    @Bean
    VehicleImageService vehicleImageService;

    @Bean
    MakeService makeService;

    @Bean
    ModelService modelService;

    @Bean
    VersionService versionService;

    @Bean
    TradeService tradeService;

    @ViewById
    CheckBox stockCheckBox;

    @ViewById
    RadioButton radioBuy;

    @ViewById
    EditText editTextKm;

    @ViewById
    LinearLayout editTextKmContainer;

    @ViewById
    EditText editTextKmFrom;

    @ViewById
    LinearLayout editTextKmFromContainer;

    @ViewById
    EditText editTextKmTo;

    @ViewById
    LinearLayout editTextKmToContainer;

    @ViewById
    EditText editTextValue;

    @ViewById
    LinearLayout editTextValueContainer;

    @ViewById
    EditText editTextValueTo;

    @ViewById
    LinearLayout editTextValueToContainer;

    @ViewById
    EditText editTextValueFrom;

    @ViewById
    LinearLayout editTextValueFromContainer;

    @ViewById
    LinearLayout contactContainer;

    @ViewById
    LinearLayout imageContainer;

    @ViewById
    FloatingActionButton takePhotoButton;

    @ViewById
    RecyclerView recyclerViewImages;

    @ViewById
    AutoCompleteTextView editTextMake;

    @ViewById
    AutoCompleteTextView editTextModel;

    @ViewById
    AutoCompleteTextView editTextVersion;

    @ViewById
    Spinner spinnerYear;

    @ViewById
    LinearLayout spinnerYearContainer;

    @ViewById
    Spinner spinnerYearFrom;

    @ViewById
    LinearLayout spinnerYearFromContainer;

    @ViewById
    Spinner spinnerYearTo;

    @ViewById
    LinearLayout spinnerYearToContainer;

    @ViewById
    Spinner spinnerFuel;

    @ViewById
    Spinner spinnerDoors;

    @ViewById
    EditText editTextNome;

    @ViewById
    EditText editTextEmail;

    @ViewById
    EditText editTextPhone;

    @ViewById
    EditText editTextDetails;

    @ViewById
    Spinner spinnerColor;

    @ViewById
    Spinner spinnerCarBody;

    @ViewById
    Spinner spinnerGearBox;

    @ViewById
    CheckBox armoredCheckBox;

    ImageAdapter imageAdapter;

    List<Make> makes = new ArrayList<>();

    Make make;

    List<Model> models = new ArrayList<>();

    Model model;

    List<Version> versions = new ArrayList<>();

    Version version;

    private ProgressDialog progress;


    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageAdapter = new ImageAdapter(this, this);
        recyclerViewImages.setAdapter(imageAdapter);
        setYearAdapter();
        configureForm();
    }

    private void setYearAdapter(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getYears());

        spinnerYear.setAdapter(adapter);
        spinnerYearFrom.setAdapter(adapter);
        spinnerYearTo.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = DialogUtil.createDialog(this, getString(R.string.discard_new_trade));
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        finish();
                    }
                });
        builder.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_trade, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.done:
                doneButtonClick();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Click({R.id.radioSell, R.id.radioBuy, R.id.stockCheckBox})
    void configureBuySellState() {
        configureForm();
    }

    @Click(R.id.takePhotoButton)
    void takePhoto() {
        captureImage();
    }

    @AfterTextChange(R.id.editTextMake)
    void makeTextChange() {
        String search = editTextMake.getText().toString();
        if (search.length() <= 1)
            return;
        searchMakes(search);
    }

    @Touch(R.id.editTextMake)
    void makeTextTouch(View touchView) {
        touchView.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(touchView, InputMethodManager.SHOW_IMPLICIT);
        setMakesAdapter();
        String search = editTextMake.getText().toString();
        searchMakes(search);
    }


    @Background
    void searchMakes(String search) {
        if (findMake(search)) {
            clearDataAfterSelectMake();
            return;
        }
        makes = makeService.searchMakes(search);
        setMakesAdapter();
    }

    private boolean findMake(String name) {
        for (Make make : makes) {
            if (name.equals(make.getName())) {
                this.make = make;
                return true;
            }
        }

        return false;
    }

    @AfterTextChange(R.id.editTextModel)
    void modelTextChange() {
        String search = editTextModel.getText().toString();
        if (search.length() <= 1)
            return;
        searchModels(search);
    }

    @Touch(R.id.editTextModel)
    void modelTextTouch(View touchView) {
        touchView.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(touchView, InputMethodManager.SHOW_IMPLICIT);
        setModelsAdapter();
        String search = editTextModel.getText().toString();
        searchModels(search);
    }

    @Background
    void searchModels(String search) {
        if(make == null)
            return;
        if (findModel(search)) {
            setDoors(search);
            versions = versionService.searchVersions("", make.getId(), model.getId());
            clearDataAfterSelectModel();
            return;
        }
        models = modelService.searchModels(search, make.getId());
        setModelsAdapter();
    }

    @UiThread
    void setDoors(String model) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.car_doors, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDoors.setAdapter(adapter);
        String doors = getDoors(model);
        if (!(doors == null)) {
            int spinnerPosition = adapter.getPosition(doors);
            spinnerDoors.setSelection(spinnerPosition);
        }
    }

    private String getDoors(String modelName){
        String toReturn = null;
        if (modelName.toLowerCase().contains("1p")) {
            toReturn = "1p";
        } else if (modelName.toLowerCase().contains("2p")) {
            toReturn = "2p";
        } else if (modelName.toLowerCase().contains("3p")) {
            toReturn = "3p";
        } else if (modelName.toLowerCase().contains("4p")) {
            toReturn = "4p";
        } else if (modelName.toLowerCase().contains("5p")) {
            toReturn = "5p";
        } else if (modelName.toLowerCase().contains("6p")) {
            toReturn = "6p";
        } else if (modelName.toLowerCase().contains("7p")) {
            toReturn = "7p";
        } else if (modelName.toLowerCase().contains("8p")) {
            toReturn = "8p";
        }

        return toReturn;
    }

    private boolean findModel(String name) {
        for (Model model : models) {
            if (name.equals(model.getName())) {
                this.model = model;
                return true;
            }
        }

        return false;
    }

    @AfterTextChange(R.id.editTextVersion)
    void versionTextChange() {
        String search = editTextVersion.getText().toString();
        searchVersions(search);
    }

    @Touch(R.id.editTextVersion)
    void versionTextTouch(View touchView) {
        touchView.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(touchView, InputMethodManager.SHOW_IMPLICIT);
        setVersionsAdapter();
        String search = editTextVersion.getText().toString();
        searchVersions(search);
    }

    void searchVersions(String search) {
        if (findVersion(search)) {
            String[] data = version.getName().split(" ");
            if (data.length == 2) {
                setValueToYear(data[0]);
                setValueToYearFrom(data[0]);
                setValueToYearTo(data[0]);
                setValueToFuel(data[1]);
            }
        }
        editTextVersion.showDropDown();
    }

    void setValueToYear(String year){
        ArrayList<String> years = getYears();


        for(int i = 0; i < years.size(); i++){
            if(StringUtil.removeAscents(year).toLowerCase()
                    .equals(StringUtil.removeAscents(years.get(i)).toLowerCase())){
                spinnerYear.setSelection(i);
                return;
            }


        }

    }

    void setValueToYearFrom(String year){
        ArrayList<String> years = getYears();


        for(int i = 0; i < years.size(); i++){
            if(StringUtil.removeAscents(year).toLowerCase()
                    .equals(StringUtil.removeAscents(years.get(i)).toLowerCase())){
                spinnerYearFrom.setSelection(i);
                return;
            }


        }

    }

    void setValueToYearTo(String year){
        ArrayList<String> years = getYears();


        for(int i = 0; i < years.size(); i++){
            if(StringUtil.removeAscents(year).toLowerCase()
                    .equals(StringUtil.removeAscents(years.get(i)).toLowerCase())){
                spinnerYearTo.setSelection(i);
                return;
            }


        }

    }

    private ArrayList<String> getYears(){
        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = thisYear+1; i >= 1990; i--) {
            if(i == thisYear+1){
                years.add("");
            }else{
                years.add(Integer.toString(i));
            }

        }
        return years;
    }

    void setValueToFuel(String fuel){
        String[] fuels = getResources().getStringArray(R.array.car_fuels);


        for(int i = 0; i < fuels.length; i++){
            if(StringUtil.removeAscents(fuel).toLowerCase()
                    .equals(StringUtil.removeAscents(fuels[i]).toLowerCase())){
                spinnerFuel.setSelection(i);
                return;
            }


        }

    }



    private boolean findVersion(String name) {
        for (Version version : versions) {
            if (name.equals(version.getName())) {
                this.version = version;
                return true;
            }
        }

        return false;
    }

    @UiThread
    void clearDataAfterSelectMake() {
        clearDataAfterSelectModel();
        editTextModel.setText("");
        spinnerDoors.setSelection(0);
        editTextVersion.setText("");
        models = new ArrayList<>();
        versions = new ArrayList<>();
        model = null;
        version = null;
        setModelsAdapter();
        setVersionsAdapter();
    }

    @UiThread
    void clearDataAfterSelectModel() {
        editTextVersion.setText("");
        version = null;
    }

    @UiThread
    void setModelsAdapter() {
        if(models == null)
            return;
        List<String> modelNames = new ArrayList<>();
        for (Model model : models) {
            modelNames.add(model.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.dropdown, modelNames);
        editTextModel.setAdapter(adapter);
        editTextModel.showDropDown();
    }


    @UiThread
    void setMakesAdapter() {
        if(makes == null)
            return;
        List<String> makeNames = new ArrayList<>();
        for (Make make : makes) {
            makeNames.add(make.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.dropdown, makeNames);
        editTextMake.setAdapter(adapter);
        editTextMake.showDropDown();
    }

    @UiThread
    void setVersionsAdapter() {
        if(versions == null)
            return;
        List<String> versionNames = new ArrayList<>();
        for (Version version : versions) {
            versionNames.add(version.getName());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.dropdown, versionNames);
        editTextVersion.setAdapter(adapter);
        editTextVersion.showDropDown();
    }


    private void captureImage() {
        PickSetup setup = new PickSetup()
                .setTitle(getString(R.string.choose))
                .setCancelText(getString(R.string.cancel))
                .setGalleryButtonText(getString(R.string.gallery))
                .setCameraButtonText(getString(R.string.camera))
                .setProgressText(getString(R.string.loading));
        PickImageDialog.build(setup).setOnPickResult(new IPickResult() {
            @Override
            public void onPickResult(PickResult r) {
                getImageFromPicker(r);
            }
        }).show(this);
    }


    private void getImageFromPicker(PickResult r) {
        Uri uri = Uri.parse(saveBitmap(r.getBitmap()));
        imageAdapter.addImage(uri);
        recyclerViewImages.setAdapter(imageAdapter);
        recyclerViewImages.setLayoutManager(new GridLayoutManager(this, 2));
        imageAdapter.notifyDataSetChanged();
        configureForm();
        recyclerViewImages.requestFocus();
    }

    private String saveBitmap(Bitmap bitmap){
        String filename = new Date().getTime() + ".png";
        File sd = Environment.getExternalStorageDirectory();
        File dest = new File(sd, filename);

        try {
            FileOutputStream out = new FileOutputStream(dest);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dest.getPath();
    }



    @Override
    public void notifyDataChange() {
        configureForm();
    }

    private void configureForm() {
        configureStockCheckBox();
        configureContactContainer();
        configureTakeImagesContainer();
        configureTakeImagesButton();
        configureContactContainer();
        configureKmFields();
        configureYear();
        configureValor();
        editTextValue.addTextChangedListener(EditTextUtil.currencyTextWatcher(editTextValue));
        editTextValueTo.addTextChangedListener(EditTextUtil.currencyTextWatcher(editTextValueTo));
        editTextValueFrom.addTextChangedListener(EditTextUtil.currencyTextWatcher(editTextValueFrom));
        editTextKm.addTextChangedListener(EditTextUtil.numberTextWatcher(editTextKm));
        editTextKmFrom.addTextChangedListener(EditTextUtil.numberTextWatcher(editTextKmFrom));
        editTextKmTo.addTextChangedListener(EditTextUtil.numberTextWatcher(editTextKmTo));
        editTextPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher("BR"));
    }


    private void configureTakeImagesContainer() {
        if (radioBuy.isChecked() || imageAdapter.getImages().size() == 0) {
            imageContainer.setVisibility(View.GONE);
        } else {
            imageContainer.setVisibility(View.VISIBLE);
        }
    }

    private void configureContactContainer() {
        if (stockCheckBox.isChecked()) {
            contactContainer.setVisibility(View.GONE);
        } else {
            contactContainer.setVisibility(View.VISIBLE);
        }
    }

    private void configureTakeImagesButton() {
        if (radioBuy.isChecked()) {
            takePhotoButton.setVisibility(View.GONE);
        } else {
            takePhotoButton.setVisibility(View.VISIBLE);
        }
    }

    private void configureValor() {
        if (radioBuy.isChecked()) {
            editTextValueContainer.setVisibility(View.GONE);
            editTextValueFromContainer.setVisibility(View.VISIBLE);
            editTextValueToContainer.setVisibility(View.VISIBLE);
        } else {
            editTextValueContainer.setVisibility(View.VISIBLE);
            editTextValueFromContainer.setVisibility(View.GONE);
            editTextValueToContainer.setVisibility(View.GONE);
        }
    }

    private void configureYear() {
        if (radioBuy.isChecked()) {
            spinnerYearContainer.setVisibility(View.GONE);
            spinnerYearFromContainer.setVisibility(View.VISIBLE);
            spinnerYearToContainer.setVisibility(View.VISIBLE);
        } else {
            spinnerYearContainer.setVisibility(View.VISIBLE);
            spinnerYearFromContainer.setVisibility(View.GONE);
            spinnerYearToContainer.setVisibility(View.GONE);
        }
    }

    private void configureKmFields() {
        if (radioBuy.isChecked()) {
            editTextKmContainer.setVisibility(View.GONE);
            editTextKmFromContainer.setVisibility(View.VISIBLE);
            editTextKmToContainer.setVisibility(View.VISIBLE);
        } else {
            editTextKmContainer.setVisibility(View.VISIBLE);
            editTextKmFromContainer.setVisibility(View.GONE);
            editTextKmToContainer.setVisibility(View.GONE);
        }
    }

    private void configureStockCheckBox() {
        User currentUser = userService.getLocalCurrentUser();
        if (null != currentUser && currentUser.getUserTypeId() == 2) {
            stockCheckBox.setVisibility(View.VISIBLE);
        } else {
            stockCheckBox.setVisibility(View.GONE);
        }
    }


    private boolean isValid() {
        if (!stockCheckBox.isChecked()) {
            if (editTextNome.getText().length() == 0 || (editTextEmail.getText().length() == 0 && editTextPhone.getText().length() == 0)) {
                showNotValidMessage(getString(R.string.not_valid), getString(R.string.insert_contact_data));
                List<Boolean> checkedList = new ArrayList<>();
                checkedList.add(ValidateUtil.isNotNullEmail(editTextEmail, getString(R.string.invalid_email)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextPhone, getString(R.string.empty_field)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextNome, getString(R.string.empty_field)));
                ValidateUtil.checkIfAllAreTrue(checkedList);
                return false;
            }
        }

        if (!radioBuy.isChecked()) {
            if (editTextMake.getText().length() == 0 || editTextModel.getText().length() == 0 || editTextVersion.getText().length() == 0) {
                showNotValidMessage(getString(R.string.not_valid), getString(R.string.insert_make_model_version));
                List<Boolean> checkedList = new ArrayList<>();
                checkedList.add(ValidateUtil.checkIsEmpty(editTextMake, getString(R.string.empty_field)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextModel, getString(R.string.empty_field)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextVersion, getString(R.string.empty_field)));
                ValidateUtil.checkIfAllAreTrue(checkedList);
                return false;
            }
        }

        if (radioBuy.isChecked()) {
            if (editTextMake.getText().length() == 0 &&
                    editTextModel.getText().length() == 0 &&
                    editTextVersion.getText().length() == 0 &&
                    spinnerDoors.getSelectedItem().toString().length() == 0 &&
                    spinnerFuel.getSelectedItem().toString().length() == 0 &&
                    editTextKmTo.getText().length() == 0 &&
                    editTextKmFrom.getText().length() == 0 &&
                    editTextValueTo.getText().length() == 0 &&
                    editTextValueFrom.getText().length() == 0 &&
                    spinnerYear.getSelectedItem().toString().length() == 0 &&
                    spinnerYearFrom.getSelectedItem().toString().length() == 0 &&
                    spinnerYearTo.getSelectedItem().toString().length() == 0 &&
                    spinnerColor.getSelectedItem().toString().length() == 0 &&
                    spinnerCarBody.getSelectedItem().toString().length() == 0 &&
                    spinnerGearBox.getSelectedItem().toString().length() == 0 &&
                    !armoredCheckBox.isChecked()) {
                showNotValidMessage(getString(R.string.not_valid), getString(R.string.insert_any_data));
                List<Boolean> checkedList = new ArrayList<>();
                checkedList.add(ValidateUtil.checkIsEmpty(editTextMake, getString(R.string.empty_field)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextModel, getString(R.string.empty_field)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextVersion, getString(R.string.empty_field)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextKm, getString(R.string.empty_field)));
                checkedList.add(ValidateUtil.checkIsEmpty(editTextValue, getString(R.string.empty_field)));
                ValidateUtil.checkIfAllAreTrue(checkedList);
                return false;
            }
        }

        return true;
    }

    private void showNotValidMessage(String title, String text) {
        DialogUtil.createDialog(this, text, title).show();
    }


    private Trade getTradeFromForm() {
        Trade trade = new Trade();
        if (radioBuy.isChecked()) {
            trade.setBusinessTypeId(Trade.BUY);
        } else {
            trade.setBusinessTypeId(Trade.SELL);
        }

        if (stockCheckBox.isChecked()) {
            trade.setStock(true);
        } else {
            trade.setStock(false);
            addContactData(trade);
        }

        Vehicle vehicle = getVehicle();

        getTradeData(trade, vehicle);

        trade.setVehicle(vehicle);

        return trade;
    }

    private void getTradeData(Trade trade, Vehicle vehicle) {

        if (radioBuy.isChecked()) {
            if (!spinnerYearFrom.getSelectedItem().toString().toString().isEmpty()) {
                vehicle.setYear(spinnerYearFrom.getSelectedItem().toString().toString());
            }
            if (!spinnerYearTo.getSelectedItem().toString().toString().isEmpty()) {
                vehicle.setYearTo(spinnerYearTo.getSelectedItem().toString().toString());
            }
        }else{
            if (!spinnerYear.getSelectedItem().toString().toString().isEmpty()) {
                vehicle.setYear(spinnerYear.getSelectedItem().toString().toString());
            }
        }

        if (!spinnerFuel.getSelectedItem().toString().toString().isEmpty()) {
            vehicle.setFuel(spinnerFuel.getSelectedItem().toString().toString());
        }

        if (!spinnerDoors.getSelectedItem().toString().toString().isEmpty()) {
            vehicle.setDoors(spinnerDoors.getSelectedItem().toString().toString());
        }

        if (!spinnerColor.getSelectedItem().toString().isEmpty()) {
            vehicle.setColor(spinnerColor.getSelectedItem().toString());
        }

        if (!spinnerCarBody.getSelectedItem().toString().isEmpty()) {
            vehicle.setCarBody(spinnerCarBody.getSelectedItem().toString());
        }

        if (!spinnerGearBox.getSelectedItem().toString().isEmpty()) {
            vehicle.setGearBox(spinnerGearBox.getSelectedItem().toString());
        }

        if (radioBuy.isChecked()) {
            if (!editTextKmFrom.getText().toString().isEmpty()) {
                vehicle.setKm(StringUtil.getCleanStringFromKM(editTextKmFrom.getText().toString()));
            }
            if (!editTextKmTo.getText().toString().isEmpty()) {
                vehicle.setKmTo(StringUtil.getCleanStringFromKM(editTextKmTo.getText().toString()));
            }
        }else{
            if (!editTextKm.getText().toString().isEmpty()) {
                vehicle.setKm(StringUtil.getCleanStringFromKM(editTextKm.getText().toString()));
            }
        }



        if (armoredCheckBox.isChecked()) {
            vehicle.setArmored(true);
        }

        if (!editTextDetails.getText().toString().isEmpty()) {
            vehicle.setDetails(editTextDetails.getText().toString());
        }

        getTradeValue(trade);
    }

    private void getTradeValue(Trade trade) {
        if (radioBuy.isChecked()) {
            if (!editTextValueFrom.getText().toString().isEmpty()) {
                Float value = StringUtil.getFloatValueFromCurrencyString(editTextValueFrom.getText().toString());
                trade.setValue(value);
            }
            if (!editTextValueTo.getText().toString().isEmpty()) {
                Float value = StringUtil.getFloatValueFromCurrencyString(editTextValueTo.getText().toString());
                trade.setValueTo(value);
            }

        }else{
            if (!editTextValue.getText().toString().isEmpty()) {
                Float value = StringUtil.getFloatValueFromCurrencyString(editTextValue.getText().toString());
                trade.setValue(value);
            }
        }
    }

    @NonNull
    private Vehicle getVehicle() {
        Vehicle vehicle = new Vehicle();
        if (make != null) {
            vehicle.setMake(make);
            if (model != null) {
                vehicle.setModel(model);
                if (version != null) {
                    vehicle.setVersion(version);
                }
            }
        }
        return vehicle;
    }

    private void addContactData(Trade trade) {
        Contact contact = new Contact();
        contact.setName(editTextNome.getText().toString());
        contact.setEmail(editTextEmail.getText().toString());
        contact.setPhone(editTextPhone.getText().toString());
        trade.setContact(contact);
    }

    private void doneButtonClick() {
        if (!isValid())
            return;
        Trade trade = getTradeFromForm();
        sendTrade(trade);

    }

    @Background
    void sendTrade(Trade trade) {
        startProgressDialod(getString(R.string.creating_trade));
        try {
            trade = tradeService.create(trade);
            closeProgressDialod();
            if (trade.getBusinessTypeId() == Trade.SELL) {
                if (imageAdapter.getImages().size() > 0) {
                    startProgressDialod(getString(R.string.uploading_images));
                    for (Uri uri : imageAdapter.getImages()) {
                        FileSystemResource image = new FileSystemResource(new File(uri.getPath()));
                        vehicleImageService.upload(trade.getVehicle(), image);
                    }
                    closeProgressDialod();
                }
            }
            closeProgressDialod();
            tradeCreated();
        } catch (Exception e) {
            closeProgressDialod();
            e.printStackTrace();
        }

    }

    @UiThread
    void startProgressDialod(String text) {
        progress = ProgressDialogUtil.callProgressDialog(this, text);
    }

    @UiThread
    void tradeCreated() {
        Toast.makeText(this, getString(R.string.trade_created), Toast.LENGTH_SHORT).show();
        finish();
    }

    @UiThread
    void closeProgressDialod() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }

}