package br.com.otgmobile.tradingcar.util;

import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by augustuscosta on 09/05/17.
 */

public class StringUtil {

    public static Float getFloatValueFromCurrencyString(String currencyString){
        if(currencyString == null || currencyString.isEmpty())
            return null;

        String cleanString = currencyString.replaceAll("[R$,.]", "");
        return Float.parseFloat(cleanString);
    }

    public static String getStringValueFromCurrencyFloat(Float currencyFloat){
        if(currencyFloat == null)
            return "";

        String formatted = NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format((currencyFloat / 100));
        return formatted;
    }

    public static String getCleanStringFromKM(String kmString){
        if(kmString == null || kmString.isEmpty())
            return null;

        return kmString.replaceAll("[,.]", "");
    }

    public static String getStringValueFromKMString(String km) {
        if(km == null || km.isEmpty())
            return null;
        int parsed = Integer.parseInt(km);
        return DecimalFormat.getInstance().format(parsed);
    }

    public static String removeAscents(String str) {
        return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
}
