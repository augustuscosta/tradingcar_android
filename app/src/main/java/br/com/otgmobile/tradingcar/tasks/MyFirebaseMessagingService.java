package br.com.otgmobile.tradingcar.tasks;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.androidannotations.annotations.EService;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.views.home.HomeActivity_;
import br.com.otgmobile.tradingcar.views.trade.ShowTradeActivity_;


/**
 * Created by augustuscosta on 25/08/16.
 */
@EService
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            JSONObject data = new JSONObject(remoteMessage.getData());
            try {
                String notificationType = data.getString("notification_type");
                if("MESSAGE".equals(notificationType)){
                    String message = data.getString("message");
                    showMessageNotification(message);
                }
                if("TRADE".equals(notificationType)){
                    String message = data.getString("message");
                    Integer tradeId = data.getInt("trade_id");
                    showMessageNotification(message, tradeId);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void showMessageNotification(String message) {
        Intent intent = HomeActivity_.intent(this).get();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        notify(message, pendingIntent);

    }

    private void showMessageNotification(String message, Integer tradeId) {
        Intent intent = ShowTradeActivity_.intent(this).tradeId(tradeId).get();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        notify(message, pendingIntent);

    }

    private void notify(String message, PendingIntent pendingIntent) {
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }


}