package br.com.otgmobile.tradingcar.service;

import android.content.Context;
import android.content.SharedPreferences;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import br.com.otgmobile.tradingcar.cloud.DeviceIdRest;
import br.com.otgmobile.tradingcar.model.DeviceId;
import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 23/06/16.
 */
@EBean
public class DeviceIdService{

    @RestService
    DeviceIdRest deviceIdRest;

    @RootContext
    Context context;

    @Bean
    SessionService sessionService;


    @Bean(CookieService.class)
    CookieService cookieUtils;

    private void saveSharedPreferencesDeviceId(String deviceid, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("DEVICEID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("DEVICEID", deviceid);
        editor.apply();
    }

    private String getSharedPreferencesDeviceId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("DEVICEID", Context.MODE_PRIVATE);
        return sharedPref.getString("DEVICEID", "");
    }

    public void updateDeviceId(String deviceId){
        saveSharedPreferencesDeviceId(deviceId, context);
    }


    public boolean sendDeviceId() {
        if(!verify())
            return false;

        boolean returnValue;
        try {

            deviceIdRest.setCookie(RestUtil.MOBILE_COOKIE, cookieUtils.acquireCookie());
            deviceIdRest.sendDeviceId(getDeviceId());
            cookieUtils.storedCookie(deviceIdRest.getCookie(RestUtil.MOBILE_COOKIE));


            returnValue = true;

        } catch (RestClientException e) {

            RestUtil.loggerForError(e);
            returnValue = false;
        }

        return returnValue;
    }

    private DeviceId getDeviceId(){
        DeviceId deviceId = new DeviceId();
        deviceId.setToken(getSharedPreferencesDeviceId(context));
        return deviceId;
    }

    private boolean verify(){
        if(getSharedPreferencesDeviceId(context) == null || "".equals(getSharedPreferencesDeviceId(context)))
            return false;

        if(sessionService.getCurrentSession() == null)
            return false;


        return true;
    }

}
