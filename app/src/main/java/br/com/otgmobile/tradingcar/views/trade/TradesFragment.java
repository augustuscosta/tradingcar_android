package br.com.otgmobile.tradingcar.views.trade;


import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.service.SearchService;
import br.com.otgmobile.tradingcar.service.TradeService;
import br.com.otgmobile.tradingcar.service.UserService;
import br.com.otgmobile.tradingcar.util.ProgressDialogUtil;

@EFragment(R.layout.fragment_trades)
public class TradesFragment extends Fragment {

    @ViewById
    RecyclerView recyclerViewTrades;

    @ViewById
    TextView emptyMessageTextView;

    @ViewById(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @Bean
    TradeService tradeService;

    @Bean
    UserService userService;

    @Bean
    SearchService searchService;

    private ProgressDialog progress;

    private boolean loading;

    private TradeAdapter tradeAdapter;


    @Click(R.id.new_trade_button)
    void newTradeButtonClick() {
        NewTradeActivity_.intent(this).start();
    }


    @Override
    public void onResume() {
        super.onResume();
        listenRefresh();
        if (!loading)
            loadComponents();


        recyclerViewTrades.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public final void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (!recyclerView.canScrollVertically(-1)) {
                    onScrolledToTop();
                } else if (!recyclerView.canScrollVertically(1)) {
                    onScrolledToBottom();
                } else if (dy < 0) {
                    onScrolledUp();
                } else if (dy > 0) {
                    onScrolledDown();
                }
            }

            public void onScrolledUp() {}

            public void onScrolledDown() {}

            public void onScrolledToTop() {}

            public void onScrolledToBottom() {
                if ("".equals(searchService.acquireSearch())){
                    loadComponents(tradeService.getAllLocal().size());
                }
            }
        });
    }

    private void listenRefresh() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadComponents();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Background()
    void loadComponents() {
        loading = true;
        startProgressDialod();
        try {
            if (!"".equals(searchService.acquireSearch())) {
                tradeService.searchCloud(searchService.acquireSearch());
                searchService.storedSearch("");
            } else {
                tradeService.getAllCloud();
            }
            setAdapterRecyclerView();
        } catch (Exception e1) {
            loading = false;
            e1.printStackTrace();
        }
        closeProgressDialod();
        loading = false;
    }

    @Background()
    void loadComponents(int size) {
        loading = true;
        startProgressDialod();
        try {
            tradeService.getAllCloud(size);
            notifyDataChange();
        } catch (Exception e1) {
            loading = false;
            e1.printStackTrace();
        }
        closeProgressDialod();
        loading = false;
    }

    @UiThread
    void setAdapterRecyclerView() {

        if (tradeService.getAllLocal().size() == 0) {
            showEmptyMessage();
            return;
        }

        recyclerViewTrades.setVisibility(View.VISIBLE);
        emptyMessageTextView.setVisibility(View.GONE);

        tradeAdapter = new TradeAdapter(getActivity(), tradeService.getAllLocal(), userService, recyclerViewTrades);
        recyclerViewTrades.setAdapter(tradeAdapter);
        recyclerViewTrades.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        tradeAdapter.notifyDataSetChanged();
    }

    @UiThread
    void notifyDataChange() {

        tradeAdapter.setTrades(tradeService.getAllLocal());
        tradeAdapter.notifyDataSetChanged();
    }

    private void showEmptyMessage() {
        recyclerViewTrades.setVisibility(View.GONE);
        emptyMessageTextView.setVisibility(View.VISIBLE);
    }

    @UiThread
    void startProgressDialod() {
        progress = ProgressDialogUtil.callProgressDialog(getActivity(), getString(R.string.loading));
    }

    @UiThread
    void closeProgressDialod() {
        ProgressDialogUtil.closeProgressDialog(progress);
    }


}
