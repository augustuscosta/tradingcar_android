package br.com.otgmobile.tradingcar.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmobile.tradingcar.cloud.UserRest;
import br.com.otgmobile.tradingcar.model.User;
import br.com.otgmobile.tradingcar.util.DatabaseHelper;
import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class UserService{

    @RootContext
    Context context;

    @RestService
    UserRest userRest;


    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<User, Integer> userDao;



    public List<User> getAll() {
        try{
            userDao.queryForAll();
            return userDao.queryForAll();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public User getLocalCurrentUser() {

        User userTemp = new User();
        try{
            QueryBuilder<User, Integer> queryBuilder = userDao.queryBuilder();
            userTemp = queryBuilder.where().eq("currentUser", true).queryForFirst();
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return userTemp;
    }

    public User getCurrentUser() throws Exception {

        User toReturn = null;

        try {
            userRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = userRest.getCurrentUser();
            toReturn.setCurrentUser(true);
            persist(toReturn);
            cookieService.storedCookie(userRest.getCookie(RestUtil.MOBILE_COOKIE));
        }catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;

    }

    public void persist(User user){
        try{
            userDao.createOrUpdate(user);
        }catch (SQLException e){
            e.printStackTrace();

        }
    }

    public User get(Integer id) {
        try{
            return userDao.queryForId(id);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


    public void clearTable() {
        try {
            TableUtils.clearTable(userDao.getConnectionSource(), User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
