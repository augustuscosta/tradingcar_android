package br.com.otgmobile.tradingcar.service;

import android.content.Context;
import android.os.Environment;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.File;

/**
 * Created by augustuscosta on 02/05/17.
 */

@EBean
public class ImageService {

    @RootContext
    Context context;

    public String createFolder() {
        String rootDir = Environment.getExternalStorageDirectory()
                + File.separator + "tradecar";
        File RootFile = new File(rootDir);
        if (!RootFile.exists())
            RootFile.mkdir();
        return rootDir;
    }
}
