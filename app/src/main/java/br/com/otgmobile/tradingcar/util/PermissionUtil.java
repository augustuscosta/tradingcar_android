package br.com.otgmobile.tradingcar.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by augustuscosta on 30/04/17.
 */

public class PermissionUtil {

    private static final int CAMERA_REQUEST = 1;

    public static boolean checkImagePermissions(Activity context){
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            context.requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST);
            return  false;
        }
        return true;
    }
}