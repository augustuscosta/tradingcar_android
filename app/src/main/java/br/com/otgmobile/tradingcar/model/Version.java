package br.com.otgmobile.tradingcar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by augustuscosta on 24/04/17.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Version {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty("id_fipe")
    private String idFipe;

    @DatabaseField
    @JsonProperty("fipe_marca")
    private String fipeMarca;

    @DatabaseField
    @JsonProperty("fipe_codigo")
    private String fipeCodigo;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty
    private String marca;

    @DatabaseField
    @JsonProperty
    private String key;

    @DatabaseField
    @JsonProperty
    private String veiculo;

    @DatabaseField
    @JsonProperty("make_id")
    private Integer makeId;

    @DatabaseField
    @JsonProperty("model_id")
    private Integer modelId;

    @ForeignCollectionField
    @JsonProperty
    private Collection<Vehicle> vehicles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdFipe() {
        return idFipe;
    }

    public void setIdFipe(String idFipe) {
        this.idFipe = idFipe;
    }

    public String getFipeMarca() {
        return fipeMarca;
    }

    public void setFipeMarca(String fipeMarca) {
        this.fipeMarca = fipeMarca;
    }

    public String getFipeCodigo() {
        return fipeCodigo;
    }

    public void setFipeCodigo(String fipeCodigo) {
        this.fipeCodigo = fipeCodigo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(String veiculo) {
        this.veiculo = veiculo;
    }

    public Integer getMakeId() {
        return makeId;
    }

    public void setMakeId(Integer makeId) {
        this.makeId = makeId;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Collection<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
