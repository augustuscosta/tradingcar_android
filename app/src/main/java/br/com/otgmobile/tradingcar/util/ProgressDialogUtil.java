package br.com.otgmobile.tradingcar.util;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by Thialyson on 03/08/16.
 */


public class ProgressDialogUtil{

    public static ProgressDialog callProgressDialog(Context context, String text) {

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(text);
        progressDialog.create();
        progressDialog.show();
        return  progressDialog;

    }

    public static void closeProgressDialog(ProgressDialog progress){
        progress.dismiss();
    }

}
