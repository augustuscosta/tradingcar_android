package br.com.otgmobile.tradingcar.service;

import android.content.Context;
import android.content.SharedPreferences;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */
@EBean
public class SearchService {

    private final String SEARCH = "search_tradingcar_app";

    @RootContext
    Context context;

    public void storedSearch(String search) {
        SharedPreferences sharedPref = context.getSharedPreferences(SEARCH, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SEARCH, search);
        editor.apply();
    }

    public String acquireSearch() {
        SharedPreferences sharedPref = context.getSharedPreferences(SEARCH, Context.MODE_PRIVATE);
        return sharedPref.getString(SEARCH, "");
    }

}
