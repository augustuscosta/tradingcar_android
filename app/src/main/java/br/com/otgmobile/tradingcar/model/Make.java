package br.com.otgmobile.tradingcar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by augustuscosta on 24/04/17.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Make {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField
    @JsonProperty("id_fipe")
    private Integer idFipe;

    @DatabaseField
    @JsonProperty
    private String name;

    @DatabaseField
    @JsonProperty("order")
    private Integer order;

    @DatabaseField
    @JsonProperty
    private String key;

    @ForeignCollectionField
    @JsonProperty
    private Collection<Vehicle> vehicles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdFipe() {
        return idFipe;
    }

    public void setIdFipe(Integer idFipe) {
        this.idFipe = idFipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Collection<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }
}
