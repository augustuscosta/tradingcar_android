package br.com.otgmobile.tradingcar.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by augustuscosta on 03/05/17.
 */

@DatabaseTable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicle {

    @DatabaseField(id = true, canBeNull = false)
    @JsonProperty
    private Integer id;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Make make;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Model model;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    @JsonProperty
    private Version version;

    @DatabaseField
    @JsonProperty
    private Boolean armored;

    @DatabaseField
    @JsonProperty
    private String details;

    @DatabaseField
    @JsonProperty
    private String km;

    @DatabaseField
    @JsonProperty("km_to")
    private String kmTo;

    @DatabaseField
    @JsonProperty
    private String year;

    @DatabaseField
    @JsonProperty("year_to")
    private String yearTo;

    @DatabaseField
    @JsonProperty
    private String fuel;

    @DatabaseField
    @JsonProperty
    private String doors;

    @DatabaseField
    @JsonProperty("car_body")
    private String carBody;

    @DatabaseField
    @JsonProperty("gear_box")
    private String gearBox;

    @DatabaseField
    @JsonProperty
    private String color;

    @ForeignCollectionField
    @JsonProperty
    private Collection<Trade> trades;

    @ForeignCollectionField
    @JsonProperty("vehicle_images")
    private Collection<VehicleImage> vehicleImages;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Make getMake() {
        return make;
    }

    public void setMake(Make make) {
        this.make = make;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public Boolean getArmored() {
        return armored;
    }

    public void setArmored(Boolean armored) {
        this.armored = armored;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getCarBody() {
        return carBody;
    }

    public void setCarBody(String carBody) {
        this.carBody = carBody;
    }

    public String getGearBox() {
        return gearBox;
    }

    public void setGearBox(String gearBox) {
        this.gearBox = gearBox;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Collection<Trade> getTrades() {
        return trades;
    }

    public void setTrades(Collection<Trade> trades) {
        this.trades = trades;
    }

    public Collection<VehicleImage> getVehicleImages() {
        return vehicleImages;
    }

    public void setVehicleImages(Collection<VehicleImage> vehicleImages) {
        this.vehicleImages = vehicleImages;
    }

    public String getKmTo() {
        return kmTo;
    }

    public void setKmTo(String kmTo) {
        this.kmTo = kmTo;
    }

    public String getYearTo() {
        return yearTo;
    }

    public void setYearTo(String yearTo) {
        this.yearTo = yearTo;
    }
}
