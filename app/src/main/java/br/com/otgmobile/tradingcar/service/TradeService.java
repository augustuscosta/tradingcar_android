package br.com.otgmobile.tradingcar.service;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import br.com.otgmobile.tradingcar.cloud.TradeRest;
import br.com.otgmobile.tradingcar.model.Contact;
import br.com.otgmobile.tradingcar.model.Make;
import br.com.otgmobile.tradingcar.model.Match;
import br.com.otgmobile.tradingcar.model.Model;
import br.com.otgmobile.tradingcar.model.Store;
import br.com.otgmobile.tradingcar.model.Trade;
import br.com.otgmobile.tradingcar.model.User;
import br.com.otgmobile.tradingcar.model.Vehicle;
import br.com.otgmobile.tradingcar.model.VehicleImage;
import br.com.otgmobile.tradingcar.model.Version;
import br.com.otgmobile.tradingcar.util.DatabaseHelper;
import br.com.otgmobile.tradingcar.util.RestUtil;


/**
 * Created by augustuscosta on 22/06/16.
 */

@EBean
public class TradeService {

    @RootContext
    Context context;

    @Bean
    UserService userService;

    @Bean
    CookieService cookieService;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Trade, Integer> dao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<User, Integer> userDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Contact, Integer> contactDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Store, Integer> storeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Vehicle, Integer> vehicleDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Make, Integer> makeDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Model, Integer> modelDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Version, Integer> versionDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<VehicleImage, Integer> vehicleImageDao;

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Match, Integer> matchDao;

    @RestService
    TradeRest tradeRest;


    public Trade create(Trade trade) throws Exception {

        Trade toReturn = null;
        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = tradeRest.create(trade);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }


    public List<Trade> getAllCloud() throws Exception {
        List<Trade> toReturn = null;
        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = tradeRest.matchs();
            clearTable();
            persist(toReturn);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    public List<Trade> getAllCloud(int size) throws Exception {
        List<Trade> toReturn = null;
        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = tradeRest.matchs(size);
            persist(toReturn);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }

    public List<Trade> searchCloud(String search) throws Exception {
        List<Trade> toReturn = null;
        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = tradeRest.search(search);
            clearTable();
            persist(toReturn);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }



    public List<Trade> getMatches(Integer tradeId) {
        List<Trade> toReturn = null;
        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            toReturn = tradeRest.getMatches(tradeId);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return toReturn;
    }
    public List<Trade> getCurrentUserBuyTrades() {

        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            List<Trade> toReturn = tradeRest.wantBuy();
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
            return toReturn;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return Collections.emptyList();

    }

    public List<Trade> getCurrentUserBuyTrades(int size) {

        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            List<Trade> toReturn = tradeRest.wantBuy(size);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
            return toReturn;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return Collections.emptyList();

    }

    public List<Trade> getCurrentUserSellTrades() {

        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            List<Trade> toReturn = tradeRest.wantSell();
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
            return toReturn;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return Collections.emptyList();

    }

    public List<Trade> getCurrentUserSellTrades(int size) {

        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            List<Trade> toReturn = tradeRest.wantSell(size);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
            return toReturn;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return Collections.emptyList();

    }

    public Trade getCloud(Integer id) {

        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            Trade toReturn = tradeRest.get(id);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
            return toReturn;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return null;
    }

    public Trade delete(Integer id) {

        try {
            tradeRest.setCookie(RestUtil.MOBILE_COOKIE, cookieService.acquireCookie());
            Trade toReturn = tradeRest.delete(id);
            cookieService.storedCookie(tradeRest.getCookie(RestUtil.MOBILE_COOKIE));
            return toReturn;
        } catch (RestClientException e) {
            RestUtil.loggerForError(e);
            RestUtil.checkUnauthorized(e, context);
        }
        return null;
    }

    public Trade getLocal(Integer id){
        try{
            return dao.queryBuilder().where().eq("id",id).queryForFirst();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public List<Trade> getAllLocal(){
        try{
            return dao.queryBuilder().orderBy("order",false).query();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private void persist(List<Trade> trades){
        if(trades == null)
            return;
        int i = getAllLocal().size();
        i++;
        for(Trade trade: trades){
            trade.setOrder(i);
            persist(trade);
            i++;
        }
    }

    private void persist(Trade trade){
        try{
            persist(trade.getUser());
            persist(trade.getContact());
            persist(trade.getStore());
            persist(trade.getVehicle());
            dao.createOrUpdate(trade);
            if(trade.getMatches() != null){
                for(Match match:trade.getMatches()){
                    match.setTrade(trade);
                    persist(match);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }


    private void persist(Match match) throws SQLException{
        if(match == null)
            return;
        matchDao.createOrUpdate(match);
    }

    private void persist(Store store) throws SQLException{
        if(store == null)
            return;
        storeDao.createOrUpdate(store);
    }

    private void persist(User user) throws SQLException {
        if(user == null)
            return;
        if(userService.getLocalCurrentUser().getId() == user.getId())
            return;
        userDao.createOrUpdate(user);
    }

    private void persist(Contact contact) throws SQLException {
        if(contact == null)
            return;
        contactDao.createOrUpdate(contact);
    }

    private void persist(Vehicle vehicle) throws SQLException {
        if(vehicle == null)
            return;
        persist(vehicle.getMake());
        persist(vehicle.getModel());
        persist(vehicle.getVersion());
        vehicleDao.createOrUpdate(vehicle);
        if(vehicle.getVehicleImages() != null){
            for(VehicleImage image:vehicle.getVehicleImages()){
                image.setVehicle(vehicle);
                persist(image);
            }
        }
    }

    private void persist(Make make) throws SQLException {
        if(make == null)
            return;
        makeDao.createOrUpdate(make);
    }

    private void persist(Model model) throws SQLException {
        if(model == null)
            return;
        modelDao.createOrUpdate(model);
    }

    private void persist(Version version) throws SQLException {
        if(version == null)
            return;
        versionDao.createOrUpdate(version);
    }

    private void persist(VehicleImage image) throws SQLException {
        if(image == null)
            return;
        vehicleImageDao.createOrUpdate(image);
    }

    public void clearTable() {
        try {
            TableUtils.clearTable(dao.getConnectionSource(), Trade.class);
            TableUtils.clearTable(makeDao.getConnectionSource(), Make.class);
            TableUtils.clearTable(modelDao.getConnectionSource(), Model.class);
            TableUtils.clearTable(versionDao.getConnectionSource(), Version.class);
            TableUtils.clearTable(matchDao.getConnectionSource(), Match.class);
            TableUtils.clearTable(contactDao.getConnectionSource(), Contact.class);
            TableUtils.clearTable(vehicleDao.getConnectionSource(), Vehicle.class);
            TableUtils.clearTable(vehicleImageDao.getConnectionSource(), VehicleImage.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
