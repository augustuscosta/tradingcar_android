package br.com.otgmobile.tradingcar.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import br.com.otgmobile.tradingcar.R;
import br.com.otgmobile.tradingcar.model.Contact;
import br.com.otgmobile.tradingcar.model.Make;
import br.com.otgmobile.tradingcar.model.Match;
import br.com.otgmobile.tradingcar.model.Model;
import br.com.otgmobile.tradingcar.model.Session;
import br.com.otgmobile.tradingcar.model.Store;
import br.com.otgmobile.tradingcar.model.Trade;
import br.com.otgmobile.tradingcar.model.User;
import br.com.otgmobile.tradingcar.model.Vehicle;
import br.com.otgmobile.tradingcar.model.VehicleImage;
import br.com.otgmobile.tradingcar.model.Version;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "tradingcar.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {


        try {

            TableUtils.createTable(connectionSource, Session.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Make.class);
            TableUtils.createTable(connectionSource, Model.class);
            TableUtils.createTable(connectionSource, Version.class);
            TableUtils.createTable(connectionSource, Store.class);
            TableUtils.createTable(connectionSource, Contact.class);
            TableUtils.createTable(connectionSource, Vehicle.class);
            TableUtils.createTable(connectionSource, VehicleImage.class);
            TableUtils.createTable(connectionSource, Trade.class);
            TableUtils.createTable(connectionSource, Match.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {


        try {

            TableUtils.dropTable(connectionSource, Session.class, true);
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, Make.class, true);
            TableUtils.dropTable(connectionSource, Model.class, true);
            TableUtils.dropTable(connectionSource, Version.class, true);
            TableUtils.dropTable(connectionSource, Store.class, true);
            TableUtils.dropTable(connectionSource, Contact.class, true);
            TableUtils.dropTable(connectionSource, Vehicle.class, true);
            TableUtils.dropTable(connectionSource, VehicleImage.class, true);
            TableUtils.dropTable(connectionSource, Trade.class, true);
            TableUtils.dropTable(connectionSource, Match.class, true);


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void close() {
        super.close();
    }
}
